//
//  BMA.h
//  BMA
//
//  Created by Cyril ATTIA on 21/12/11.
//  Copyright (c) 2012 BeMyApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BMADelegate.h"

#define BMA_BANNER_FULL 0x000
#define BMA_BANNER_TXT 0x001
#define BMA_BANNER_IMAGE 0x002

#define MODE_BANNER @"BANNER"
#define MODE_SIMPLE_VIEW @"SIMPLE_VIEW"

#define AC_OPTIONALLY_INVOKE_DELEGATE(_delegate, _selector)	\
{ \
id delegateAsId = (id)_delegate;								\
if([delegateAsId respondsToSelector:@selector(_selector)])	\
{														\
[delegateAsId performSelector:@selector(_selector)];	\
} \
}

#define AC_OPTIONALLY_INVOKE_DELEGATE_WITH_PARAMETER(_delegate, _selector, _parameter)	\
{ \
id delegateAsId = (id)_delegate;								\
if([delegateAsId respondsToSelector:@selector(_selector)])	\
{														\
[delegateAsId performSelector:@selector(_selector) withObject:(id)(_parameter)];	\
} \
}

#define AC_OPTIONALLY_INVOKE_DELEGATE_WITH_TWO_PARAMETERS(_delegate, _selector, _parameterOne, _parameterTwo)	\
{ \
id delegateAsId = (id)_delegate;								\
if([delegateAsId respondsToSelector:@selector(_selector)])	\
{														\
[delegateAsId performSelector:@selector(_selector) withObject:(id)(_parameterOne) withObject:(id)(_parameterTwo)];	\
} \
}

@class FullscreenController;
@class BrowserController;

@interface BMA : NSObject <BMADelegate,UIWebViewDelegate>
{
    id<BMADelegate> delegate;
}

@property (nonatomic, assign) id<BMADelegate> delegate;



////////////////////////////////////////////////////////////
///
/// @return The version of the BMA client library in use.
///
////////////////////////////////////////////////////////////
+ (NSUInteger)versionNumber;

////////////////////////////////////////////////////////////
///
/// @return The release Version String of the BMA client library in use.
///
////////////////////////////////////////////////////////////
+ (NSString*)releaseVersionString;

////////////////////////////////////////////////////////////
///
/// @param appKey is copied. This is your unique application key you received when registering your application.
///
/// @note This will begin the application authorization process.
///
////////////////////////////////////////////////////////////
+ (BMA*)initializeWithAppKey:(NSString *)appKey;

////////////////////////////////////////////////////////////
///
/// @param set sdk delegate
///
////////////////////////////////////////////////////////////
+(void)setDelegate:(id<BMADelegate>) delegate;

////////////////////////////////////////////////////////////
///
/// @param rate the fullscreen display rate in seconds (default 120s, min 30s)
///
////////////////////////////////////////////////////////////
+(void)setFsRate:(long) rate;
    
////////////////////////////////////////////////////////////
///
/// @note This will launch the fullscreen ad.
///
////////////////////////////////////////////////////////////
- (void)launchFullScreen;

- (void)launchFullScreenWithController:(UIViewController*) controller;

////////////////////////////////////////////////////////////
///
/// @param reversed NO (default) -> top banner YES -> bottom banner
///
/// @note use this method to display banner at bottom of the view
///
////////////////////////////////////////////////////////////
+ (void)setBottomBanner:(BOOL)reversed;

////////////////////////////////////////////////////////////
///
/// @param type (BMA_BANNER_TXT, BMA_BANNER_IMAGE, BMA_BANNER_FULL)
///
////////////////////////////////////////////////////////////
+ (void)setBannerType:(int)type;

////////////////////////////////////////////////////////////
///
/// @param timeout the banner refresh timeout in seconds (default 15mn, min 2mn)
///
////////////////////////////////////////////////////////////
+ (void)setBannerTimeout:(double) timeout;

////////////////////////////////////////////////////////////
///
/// @param margin the banner margin in pixels (default 0)
///
////////////////////////////////////////////////////////////
+ (void)setBannerMargin:(int) margin;

////////////////////////////////////////////////////////////
///
/// @param controller
///
/// @note use this method to add banner in a view
///
////////////////////////////////////////////////////////////
+ (void)addBannerWithController:(UIViewController *) controller;
//- (void)addBannerWithController:(UIViewController *) controller;

////////////////////////////////////////////////////////////
///
/// @param the size of the container wihch will be display the
///
////////////////////////////////////////////////////////////
+ (UIView *)fetchAdsWithSize:(CGSize)size;

////////////////////////////////////////////////////////////
///
/// @note  Giving back the ads generated.
///
////////////////////////////////////////////////////////////
+ (void)giveBackAds:(UIView *)view;

////////////////////////////////////////////////////////////
///
/// @note	Use this method if the number of ads displayed is more than 2.
///
///	 /!\	WARING a big number will cause a significant lack of performance due to the number of request.
///
////////////////////////////////////////////////////////////
+ (void)setMinAdsPreloads:(NSInteger)min;

+ (void)setMode:(NSString *)string;

+ (void) removeBanner;

+ (void) forceBannerRefresh;

+ (void) redrawBanner;

+ (void) setLandscapeMode:(BOOL) b;

@end
