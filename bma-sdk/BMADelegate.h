//
//  BMAAppDelegate.h
//  BMA
//
//  Created by Cyril ATTIA on 21/12/11.
//  Copyright (c) 2012 BeMyApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol BMADelegate<UIWebViewDelegate>

@optional

#pragma mark -
#pragma mark GENERAL

// application linked to campaign
- (void) applicationLinked;
// application not linked to campaign
- (void) applicationNotLinked;

#pragma mark -
#pragma mark BANNER

// the banner is successfully loaded
-(void) bannerViewDidLoadAd:(UIView*)banner;

// failed to load the banner
-(void) bannerViewDidFailToReceiveAd:(UIView*)banner withError:(NSError*)error;

// Called before a banner view executes an action (banner click)
-(BOOL) bannerViewActionShouldBegin:(UIView*)banner willLeaveApplication:(BOOL)willLeave;

// Called after a banner view finishes executing an action that covered your application’s user interface.
-(void) bannerViewActionDidFinish:(UIView*)banner;

// Called when an expandable ad did unexpand (duration time elapsed).
-(void) expandDidUnexpand:(UIView*) banner;

#pragma mark -
#pragma mark FULLSCREEN

// Called when an intersticial view will appear
-(void) intersticialWillAppear;

// Called when an intersticial view did appear
-(void) intersticialDidAppear;

// Called when an intersticial view will disappear
-(void) intersticialWillDisappear;

// Called when an intersticial view did disappear
-(void) intersticialDidDisappear;

@end

