#README BMA SANDBOX.

##Presentation
This project allows you to make tests with your own BMA to see the rendering on ur futur app.

It also show you how to integrate the sdk for each mode.

##Setup Requierements
To use our SDK you will need to add some dependencies to your project. Make sure you have those Frameworks :

	- Foundation
	- CFNetwork
	- SystemConfiguration
	- UIKit
	- CoreGraphics
	- AVFoundation
	- MediaPlayer

Don't forget to correctly put flags on each linkers : 
For that go to the build settings of your project search for :"Other linker Flags" and add the option "-all_load"

##Full screen configuraiton
``` objective-c
//AppDelegate.h
@interface AppDelegate : NSObject<UIApplicationDelegate, BMADelegate>
#import "BMA.h"
@property (nonatomic, strong) IBOutlet BMA *pub;
```

``` objective-c
//AppDelegate.m
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
	_pub = [BMA initializeWithAppKey:@"yourKey"];
    return YES;
}
```

To have the fullscreen interstitial, in the DidBecomeActive() delegate method, add the following line of code :
``` objective-c
- (void)applicationDidBecomeActive:(UIApplication *)application{
	 [_pub launchFullScreen];
}	
```

To know if a user is sponsorized just add a method : 
``` objective-c
- (void)applicationLinked;
```

##Banner configuration

Add a dependency to your ViewController :
``` objective-c
#import "BMA.h"

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[BMA setMode:BANNER_MODE];
	[BMA setBannerType:BMA_BANNER_FULL];
	[BMA setBottomBanner:NO];
	[BMA setBannerMargin:0];
	[BMA addBannerWithController:self];
}
```

This is the addBannerWithController method which will starts the algorithm/requests to display the banner in your controller.

Don't forget to remove the banner in the ViewWillDisappear method: 
``` objective-c
- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[BMA removeBanner];
}
```

The banner as multiple settings depending your applicaiton

1. Reverse the position.
The defaut behavior of the banner is to slide from the of the screen to the bottom. If you wish to reverse this behavior put this line of code during the banner initialization :

``` objective-c
    [BMA setBottomBanner:YES];
```

2. Add a padding. 
The banner position can receive an offset. You just have to put this line of code : 

``` objective-c
    [BMA setBannerMargin:20];
```

3. Choose the banner type.

To display images only : 
``` objective-c
	[BMA setBannerType:BMA_BANNER_IMAGE];
```

To display text only : 
``` objective-c
    [BMA setBannerType:BMA_BANNER_TXT];
```

To display both : 
``` objective-c
   [BMA setBannerType:BMA_BANNER_FULL];
```

- TIPS: 
	If you want to recreate the banner after a particular event (a change of displacement with margins, changing direction or changing the type of banners) you can use redrawBanner.

``` objective-c
   [BMA redrawBanner];
```

## Safety integration
The sponsorship provides for:
	- Fullscreen display at startup and the reactivation of the application;
	- An expand-banner on the main view (BMA_BANNER_FULL);
	- Some text banners (BMA_BANNER_TXT) or images (BMA_BANNER_IMAGE) on the subviews.