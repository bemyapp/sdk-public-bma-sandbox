//
//  CustomViewController.m
//  Foo
//
//  Created by Julien Sarazin on 07/03/13.
//  Copyright (c) 2013 Julien Sarazin. All rights reserved.
//

#import "CustomViewController.h"
#import "AppDelegate.h"
#import "BMA.h"

@interface CustomViewController ()

@property (strong, nonatomic) NSString *currentMode;
@property BOOL bottomBannerActivated;

@end

@implementation CustomViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        #pragma warning setup : Change the mode here to see the differences.
        _currentMode = MODE_BANNER;
        _bottomBannerActivated = NO;
        #pragma warning setup : This line of code is needed to tell the SDK in which mode it is allowed to respond.
        [BMA setMode:_currentMode];
        
        #pragma warning setup : Those lines of code is needed to animate the banner and set its position. (BANNER MODE ONLY)
        [BMA setBottomBanner:_bottomBannerActivated];
        [BMA setBannerType:BMA_BANNER_TXT];
        [BMA setBannerMargin:0];
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            self.navigationController.navigationBar.translucent = _bottomBannerActivated;
        }
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    UIScrollView *scv = (UIScrollView*)self.view.subviews[0];
    scv.contentSize = CGSizeMake(320, 640);
	//_currentMode = MODE_SIMPLE_VIEW; // Only one mode can being set for the SDK.
}

- (void)viewWillAppear:(BOOL)animated
{
	[BMA initializeWithAppKey:@"demoBMA"]; //not required if already initialized in app delegate (used here to switch to an in-app campaign)
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
    #pragma warning setup : This line of code is needed to tell the SDK in which mode it is allowed to respond.
    [BMA setMode:_currentMode];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.navigationController.navigationBar.translucent = _bottomBannerActivated;
    }
    
	[BMA addBannerWithController:self]; // This is this method call which will starts the banner displaying algorithm.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [BMA removeBanner];
	[super viewWillDisappear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [BMA removeBanner];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [BMA removeBanner];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [BMA addBannerWithController:self];
}

- (IBAction)onTouchBtnBannerTXT:(id)sender
{
	[BMA removeBanner];
	[BMA setBannerType:BMA_BANNER_TXT];
	//[BMA redrawBanner];
	[BMA addBannerWithController:self];
}

- (IBAction)onTouchBtnBannerIMG:(id)sender
{
	[BMA removeBanner];
	[BMA setBannerType:BMA_BANNER_IMAGE];
	//[BMA redrawBanner];
	[BMA addBannerWithController:self];
}

- (IBAction)onTouchBtnBannerFULL:(id)sender
{
	[BMA removeBanner];
	[BMA setBannerType:BMA_BANNER_FULL];
	//[BMA redrawBanner];
	[BMA addBannerWithController:self];
}

- (IBAction)onTouchBtnRemoveBanner:(id)sender
{
    [BMA removeBanner];
}

- (IBAction)onTouchBtnLaunchFullScreen:(id)sender
{
	// little bit dirty but shouldn't being called
	[((AppDelegate *)[UIApplication sharedApplication].delegate).pub launchFullScreen];
}

- (IBAction)onTouchSwapBtnPosition:(id)sender
{
	[BMA removeBanner];
	_bottomBannerActivated = !_bottomBannerActivated;
	[BMA setBottomBanner:_bottomBannerActivated];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.navigationController.navigationBar.translucent = _bottomBannerActivated;
    }
	[BMA addBannerWithController:self];
	
	//[BMA redrawBanner];
}
@end
