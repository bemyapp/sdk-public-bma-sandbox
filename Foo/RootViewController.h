//
//  ViewController.h
//  Foo
//
//  Created by Julien Sarazin on 04/03/13.
//  Copyright (c) 2013 Julien Sarazin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
- (IBAction)onTouchBtnBannerTXT:(id)sender;
- (IBAction)onTouchBtnBannerIMG:(id)sender;
- (IBAction)onTouchBtnBannerFULL:(id)sender;
- (IBAction)onTouchBtnRemoveBanner:(id)sender;

- (IBAction)onTouchBtnLaunchFullScreen:(id)sender;
- (IBAction)onTouchSwapBtnPosition:(id)sender;

@end
