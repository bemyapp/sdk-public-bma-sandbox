//
//  ViewController.m
//  Foo
//
//  Created by Julien Sarazin on 04/03/13.
//  Copyright (c) 2013 Julien Sarazin. All rights reserved.
//

#import "RootViewController.h"
#import "AppDelegate.h"
#import "BMA.h"


#define BigCellSize_Landscape CGSizeMake(389, 480);
#define BigCellSize_Portrait CGSizeMake(634, 186);

#define SmallCellSize_Landscape CGSizeMake(395, 235);
#define SmalCellSize_Portrait CGSizeMake(312, 142);

#define EdgeInsets_Landscape UIEdgeInsetsMake(0, 10, 50, 0);
#define EdgeInsets_Portrait UIEdgeInsetsMake(0, 40, 10, 40);
#define LineSpacing 10
#define CellSpacing 10

@interface RootViewController ()

@property (strong, nonatomic) NSString *currentMode;
@property BOOL bottomBannerActivated;

-(void)setupUIWithInterfaceOrientation:(UIInterfaceOrientation)orientation
							  duration:(NSTimeInterval)duration;
@end

@implementation RootViewController

-(void)setupUIWithInterfaceOrientation:(UIInterfaceOrientation)orientation
							  duration:(NSTimeInterval)duration

{
	self.collectionView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	if (self.collectionView.backgroundView == nil)
		self.collectionView.backgroundView = [[UIView alloc] init];
	
	UIView *collectionViewBackground = [self.collectionView.backgroundView.subviews lastObject];
	UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
	
	if (UIDeviceOrientationIsPortrait(orientation))
	{
		layout.scrollDirection = UICollectionViewScrollDirectionVertical;
		self.collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, 2);
		
		if (collectionViewBackground != nil)
			[collectionViewBackground removeFromSuperview];
		
		[self.collectionView.backgroundView addSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"CK__scrollBar_portrait_background_mask.png"]]];
	}
	else
	{
		layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
		self.collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 2, 0);
		
		if (collectionViewBackground != nil)
			[collectionViewBackground removeFromSuperview];
		
		[self.collectionView.backgroundView addSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"CK__scrollBar_background_mask.png"]]];
	}
	
	[self.collectionView setCollectionViewLayout:layout];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.collectionView.delegate = self;
	self.collectionView.dataSource = self;
	
#pragma warning setup : Swap the mode here to see the differences.
	_currentMode = MODE_BANNER;
    [BMA setBottomBanner:NO];
    [BMA setBannerType:BMA_BANNER_IMAGE];
    [BMA setBannerMargin:0];
	//_currentMode = MODE_SIMPLE_VIEW; // Only one mode can being set for the SDK.
}

- (void)viewWillAppear:(BOOL)animated
{
	UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
	[self setupUIWithInterfaceOrientation:orientation duration:0];
	
	[super viewWillAppear:animated];
#pragma warning setup : This line of code is needed to tell the SDK in which mode it is allowed to respond.
	[BMA setMode:_currentMode];
	
#pragma warning setup : Those lines of code is needed to animate the banner and set its position. (BANNER MODE ONLY)
	if ([_currentMode isEqualToString:MODE_BANNER])
	{
		
		[BMA addBannerWithController:self]; // This is this method call which will starts the banner displaying algorithm.
	}
	else
	{
		[BMA setMinAdsPreloads:5]; // For each size claimed we clone it and cache them min times. Usefull when you may display more than one ads per screen
	}
}

- (void)viewDidAppear:(BOOL)animated
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.navigationController.navigationBar.translucent = false;
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	if([_currentMode isEqualToString:MODE_BANNER])
		[BMA removeBanner];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Orientation handlers -
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	[self setupUIWithInterfaceOrientation:toInterfaceOrientation duration:duration];
	[super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [BMA removeBanner];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [BMA addBannerWithController:self];
	[self.collectionView.collectionViewLayout invalidateLayout];
	[self.collectionView.collectionViewLayout prepareLayout];
	[self.collectionView reloadData];
}

#pragma mark - UICollectionView Data Source -
#pragma mark Getting Item and Section Metrics
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView*)collectionView
{
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	if(section == 0)
		return 1;
	
	return 10;
}

#pragma mark Getting Views For Items
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	UICollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"cell"
																				forIndexPath:indexPath];
				
	cell.backgroundColor = [UIColor colorWithHue:25 saturation:25 brightness:25 alpha:0.33];
	
	if ([_currentMode isEqualToString:MODE_SIMPLE_VIEW])
	{
		if(cell.contentView.subviews.count > 0)
		{
			[BMA giveBackAds:cell.contentView.subviews[0]]; // push back the view into the SDK cache ads stack. (usefull for performance improvements)
			[cell.contentView.subviews[0] removeFromSuperview];
		}
	
		if(indexPath.row == 0 || indexPath.row == 5 || indexPath.row == 9)
			[cell.contentView addSubview:[BMA fetchAdsWithSize:cell.contentView.frame.size]]; // pop a view at the required size from the cache ads stack.
	}
	
	return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
	return nil;
}

#pragma mark - CollectionViewFlowLayout Delegate -
#pragma mark Getting the size of items
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
	UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
	if (indexPath.section == 0)
	{
		if (UIDeviceOrientationIsPortrait(orientation))
			return BigCellSize_Portrait;
		return BigCellSize_Landscape;
	}

	else
	{
		if (UIDeviceOrientationIsPortrait(orientation))
			return SmalCellSize_Portrait;
		
		return SmallCellSize_Landscape;
	}

}

#pragma mark Getting the sections spacing
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
	return CellSpacing;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
	return LineSpacing;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
	
	if(UIDeviceOrientationIsPortrait(orientation))
		return EdgeInsets_Portrait;
	
	return EdgeInsets_Landscape;
}

- (IBAction)onTouchBtnBannerTXT:(id)sender
{
	[BMA removeBanner];
	[BMA setBannerType:BMA_BANNER_TXT];
	//[BMA redrawBanner];
	[BMA addBannerWithController:self];
}

- (IBAction)onTouchBtnBannerIMG:(id)sender
{
	[BMA removeBanner];
	[BMA setBannerType:BMA_BANNER_IMAGE];
	//[BMA redrawBanner];
	[BMA addBannerWithController:self];
}

- (IBAction)onTouchBtnBannerFULL:(id)sender
{
	[BMA removeBanner];
	[BMA setBannerType:BMA_BANNER_FULL];
	//[BMA redrawBanner];
	[BMA addBannerWithController:self];
}

- (IBAction)onTouchBtnRemoveBanner:(id)sender
{
    [BMA removeBanner];
}

- (IBAction)onTouchBtnLaunchFullScreen:(id)sender
{
	// little bit dirty but shouldn't being called
	[((AppDelegate *)[UIApplication sharedApplication].delegate).pub launchFullScreen];
}

- (IBAction)onTouchSwapBtnPosition:(id)sender
{
	[BMA removeBanner];
	_bottomBannerActivated = !_bottomBannerActivated;
	[BMA setBottomBanner:_bottomBannerActivated];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.navigationController.navigationBar.translucent = _bottomBannerActivated;
    }
	[BMA addBannerWithController:self];
	
	//[BMA redrawBanner];
}
@end
