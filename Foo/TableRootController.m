//
//  TableRootController.m
//  Foo
//
//  Created by Julien Sarazin on 04/03/13.
//  Copyright (c) 2013 Julien Sarazin. All rights reserved.
//

#import "TableRootController.h"

@interface TableRootController ()

@property (strong, nonatomic) NSString *currentMode;
@end

@implementation TableRootController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
#pragma warning setup : Change the mode here to see the differences.
	//_currentMode = MODE_BANNER;
	_currentMode = MODE_SIMPLE_VIEW; // Only one mode can being set for the SDK.
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
#pragma warning setup : This line of code is needed to tell the SDK in which mode it is allowed to respond.
    [BMA initializeWithAppKey:@"demoBeMyBlog"]; //not required if already initialized in app delegate (used here to switch to a blog feed campaign)
    
	[BMA setMode:_currentMode];
    [BMA setBannerType:BMA_BANNER_IMAGE];
	
#pragma warning setup : Those lines of code is needed to animate the banner and set its position. (BANNER MODE ONLY)
	if([_currentMode isEqualToString:MODE_BANNER])
	{
		[BMA setBottomBanner:NO];
		[BMA setBannerMargin:0];
	}
	
	[BMA addBannerWithController:self]; // This is this method call which will starts the banner displaying algorithm.
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [BMA removeBanner];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [BMA removeBanner];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 25;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
   
	cell.backgroundColor = [UIColor colorWithHue:25 saturation:25 brightness:25 alpha:0.33];

	if ([_currentMode isEqualToString:MODE_SIMPLE_VIEW])
	{
		if(cell.contentView.subviews.count > 0)
		{
			[BMA giveBackAds:cell.contentView.subviews[0]]; // push back the view into the SDK cache ads stack. (usefull for performance improvements)
			[cell.contentView.subviews[0] removeFromSuperview];
		}
	
		if(indexPath.row == 0 || indexPath.row == 5 || indexPath.row == 9)
			[cell.contentView addSubview:[BMA fetchAdsWithSize:cell.frame.size]]; // pop a view at the required size from the cache ads stack.
	}
	
	return cell;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
	[self.tableView reloadData];
}
@end
