//
//  AppDelegate.h
//  Foo
//
//  Created by Julien Sarazin on 04/03/13.
//  Copyright (c) 2013 Julien Sarazin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BMA.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, BMADelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) BMA *pub;

@end
