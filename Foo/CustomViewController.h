//
//  CustomViewController.h
//  Foo
//
//  Created by Julien Sarazin on 07/03/13.
//  Copyright (c) 2013 Julien Sarazin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomViewController : UIViewController
- (IBAction)onTouchBtnBannerTXT:(id)sender;
- (IBAction)onTouchBtnBannerIMG:(id)sender;
- (IBAction)onTouchBtnBannerFULL:(id)sender;
- (IBAction)onTouchBtnRemoveBanner:(id)sender;

- (IBAction)onTouchBtnLaunchFullScreen:(id)sender;
- (IBAction)onTouchSwapBtnPosition:(id)sender;


@end
